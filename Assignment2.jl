### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ c70c8b04-166a-480c-acb7-7a4efcb0930f
using DataFrames

# ╔═╡ 6d41d5f0-2d61-4240-bc90-88f68c7bff56
using Random

# ╔═╡ 4ccdb7fe-0b24-4905-8939-8c580cbf3102
using Parameters

# ╔═╡ 504ff72a-07dd-49e8-ac4d-952f3e59e536
using Setfield

# ╔═╡ 396ec0e2-c5a8-11ec-2c94-b9b42bf7d13b
md"## Problem 1"

# ╔═╡ 283bfa9e-833b-4754-b8b3-2229c8b2aafc
md"""User input"""

# ╔═╡ 1e527b64-9100-4a70-82f8-02c95835d059
begin
	no_of_storeys = 3
	occ_offices = 4
	parcels = 1
	agent_start = [1,1]
end

# ╔═╡ 8f52bbd5-e4b1-4e24-a5ea-7bbebbb47f50
begin
test = []
x= 0
for i in range(1, 3) 
	test.append([])  
    for j in range(1, 4) 
          nums[i].append(j)
    end

end
print(test)
end

# ╔═╡ 15b0d459-6e9b-4493-834e-5a8dbe28a855
test1 = zeros(Int8, 3, 3)

# ╔═╡ eb4ea3c8-c5af-4731-b4cb-98a9835e3963
rand(Int8, (3,3))

# ╔═╡ 0f611ea5-429c-4ced-84a2-476654ca3441
begin
rng = MersenneTwister(1234);
bitrand(rng, 5)
end

# ╔═╡ f442268a-0420-4d10-91da-b6c3a0b601bf
md"""First we have to create functions that will contain all the attributes associated with a node. 
1. Parent of node
2. Position of the node
3. All three costs (g,h & f)

* Function one 'init' will initialise the node.
* Function two 'eq' will check the equality of the node with another node."""

# ╔═╡ d8fce66d-7e34-46dd-ba5d-efa5c814a271
mutable struct office
	id::Tuple{Int64, Int64} 	#special id for office
	parcels::Int8   	# no of parcels in office
	neighbourUp::Bool 	# does it have neighbours up, down, left, right
	neighbourDown::Bool
	neighbourE::Bool
	neighbourW::Bool
end

# ╔═╡ dbf62d40-918a-40ad-8f5e-df9f77770925
mutable struct transition
	Type::Tuple{Int64, Int64}
	Cost:: Int64
end

# ╔═╡ b59ce9ca-719f-45b1-85bd-c0835dfdef0b
mutable struct Agent
	parent::Tuple{Int64, Int64}
	position::Tuple{Int64, Int64}
end

# ╔═╡ 16dde0f6-aee0-4255-b6e9-0491742fdd7b
begin
	o1 =office((1,1), 1, true, false, false, true )
	o1
end

# ╔═╡ bdd0b535-e129-40d4-8468-6bbc802e8dc5
mutable struct Cost
	g::Int64
	h::Int64
	f::Int64
end

# ╔═╡ ee527978-ac79-49a3-84ab-828788cb0964
mutable struct action
	down::Tuple{Int64, Int64} 
	up::Tuple{Int64, Int64}
	east::Tuple{Int64, Int64}
	west::Tuple{Int64, Int64}
end

# ╔═╡ 13b277c3-3227-46dc-a672-4d2542cecbe5
@enum Actions go_down=(0, -1) go_up=(-1, 0) go_east=(0, 1) go_west=(1, 0)


# ╔═╡ ee826cb5-2d51-4518-90a8-c682db3d4c62
	function init( parent , position)
		parent = parent
		position = position

		Cost.g = 0
		Cost.h = 0
		Cost.f = 0

		action.down = (0,-1)
		action.up 	= (-1,0)
		action.east = (0,1)
		action.west = (1,0)
		
	end

# ╔═╡ ce318d63-5ad2-4741-bd1d-ca96d6e286d5
function building(storeys::Int64 , occ_offices::Int64 , parcels::Int64 ) # s = 3, s = of = 4, p = 1
	space = zeros(Int8, storeys, 3)

	
	"""
	building = [0 1 0;1 0 1; 0 1 0]
	building
	"""
	return space
end

# ╔═╡ fc7e7288-e49b-4e63-9247-e684c6bd5be9
begin
	building_test = building(3,3,3)
	building_test[CartesianIndex.(1:2, 2:2)] .= 1;

	building_test
end

# ╔═╡ eb2a0e23-4c32-455d-a56e-e9cb6792c49a
begin
	test_mat2 = [0 1 0]
	test_mat2[CartesianIndex.(Tuple(1,1))] .= 1
	#test_mat2[1,1], test_mat2[1,2], test_mat2[1,3]

end

# ╔═╡ 5343b156-b0b7-41c0-9809-4564c22be5d0
begin
	test_mat = [0 1 0]
	val = replace!([test_mat[1,1]], 0=>1, count = 1)
	#test_mat[1,1] = val
end

# ╔═╡ 4e5770e5-83e4-4f6a-b33c-cad4a5c87f3b
begin
b = a + 1
b	
end

# ╔═╡ 2f97b7c2-d1cf-4f40-81b0-9b9ca32a2fca
function eq(self, other)
		return self.position ==other.position
	end

# ╔═╡ da393251-b601-4f5a-8d3e-82eba0655983
md"""Next we define a path function:
* Returns the path from the start node to the target node (end node)"""

# ╔═╡ 9b4928b9-e4b9-41bd-a95b-d9877f47b90f
begin 
	function return_path(current_node, building)
		path = []
		no_rows, no_columns = size(building)
		# create initialised result building with -1 in every position
		result = [[-1 for i in range(no_column)] for j in range(no_rows)]
		current = current_node
		while current != nothing
			path.append(current.position)
			current = current.parent
			# Return reversed path to show start to end path
			path = reverse(path)
			start_value = 0
		end

			#update the path of start to end found by A* search with every step incremented
		for i in range(len(path))
			result[path[i][0]][path[i][1]] = start_value
				start_value +=1
		end
		return result 
		
	end
end 

# ╔═╡ 32339ebd-eea9-431c-a990-9e7ada45a58a
md"""Next we define the search function"""

# ╔═╡ 9e8d0d3e-40e8-40d3-bbc1-6f2574c76a0c
function search(building, cost, start, goal)

	#Create start and end node with initialised values for g, h, f
	start_node = Node(:, :, tuple(start))
	start_node.g = start_node.h = start_node.f = 0
	end_node = Node(nothing, tuple(goal))
	end_node.g = end_node.h = end_node.f = 0

	#Initialise visited and not yet visited lists
	#this list stores nodes not visited yet 
	not_visited_list = []
	#list of visited nodes
	visited_list = []
	#Add the start node
	not_visited_list.append(start_node)

	#Stop condition to avoid infinite loops and stop execution after a reasonable amount of steps
	outer_iterations = 0
	max_iterations = (len(building) // 2) ^ 10

	#Define actions. move-east, move-west, move-up, move-down

	move = [[-1, 0], #go up
			[0, -1], #go down
			[1, 0], #go left (west)
			[0, 1]] #go right (east)

	no_rows, no_columns = size(building)

	#Loop until you find the end

	while len(not_visited_list) > 0
		outer_iterations += 1

		#get current node
		current_node = not_visited_list[0]
		current_index = 0
		for (index, item) in enumerate(not_visited_list)
			if (item.f < current_node.f)
				current_node = item
				current_index = index
			end
		end

		if outer_iterations > max_iterations
            print("giving up on pathfinding too many iterations")
			return return_path(current_node, building)
		end

		# Pop current node out off yet_to_visit list, add to visited list
        not_visited_list.pop(current_index)
        visited_list.append(current_node)

        # test if goal is reached or not, if yes then return the path
        if (current_node == end_node)
            return return_path(current_node, building)
		end

		# Generate children from all adjacent squares
        children = []

        for new_position in move

            # Get node position
            node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

			# Make sure within range (check if within maze boundary)
            if (node_position[0] > (no_rows - 1) || 
                node_position[0] < 0 || 
                node_position[1] > (no_columns -1) || 
                node_position[1] < 0);
                continue
			end

			# Make sure walkable terrain
            if building[node_position[0]][node_position[1]] != 0
                continue
			end
		
            # Create new node
            new_node = Node(current_node, node_position)
		end
		

            # Append
            children.append(new_node)

		# Loop through children
        for child in children
            
            # Child is on the visited list (search entire visited list)
            if len([visited_child for visited_child in visited_list if visited_child == child]) > 0
                continue
			end
			
            # Create the f, g, and h values
            child.g = current_node.g + cost
			
            ## Heuristic costs calculated here, this is using eucledian distance
            child.h = (((child.position[0] - end_node.position[0]) ^ 2) + 
                       ((child.position[1] - end_node.position[1]) ^ 2)) 

            child.f = child.g + child.h

            # Child is already in the yet_to_visit list and g cost is already lower
            if len([i for i in not_visited_list if child == i & child.g > i.g]) > 0;
                continue
			end

            # Add the child to the yet_to_visit list
            not_visited_list.append(child)
		end
	end
end

# ╔═╡ 130d554f-765c-4840-a254-ec7fae6a4dd3
begin
	

    maze = [[0, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 0],
            [0, 0, 0, 0, 1, 0]]
	
    start1 = [1,1 ] # starting position
    goal1 = [4,5] # ending position
    cost1 = 1 # cost per movement

    path = search(maze ,cost1, start1, goal1)
    print(path)
end

# ╔═╡ 93f7beef-0a81-4cb5-939b-1a3707a573c1


# ╔═╡ 83ba4dc7-b735-4c7c-aecb-3cec6b27e669


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
Parameters = "d96e819e-fc66-5662-9728-84c9c7592b0a"
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"
Setfield = "efcf1570-3423-57d1-acb7-fd33fddbac46"

[compat]
DataFrames = "~1.3.3"
Parameters = "~0.12.3"
Setfield = "~0.8.2"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "fb5f5316dd3fd4c5e7c30a24d50643b73e37cd40"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.10.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "Future", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrettyTables", "Printf", "REPL", "Reexport", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "6c19003824cbebd804a51211fd3bbd81bf1ecad5"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.3.3"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "bee5f1ef5bf65df56bdd2e40447590b272a5471f"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.1.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parameters]]
deps = ["OrderedCollections", "UnPack"]
git-tree-sha1 = "34c0e9ad262e5f7fc75b10a9952ca7692cfc5fbe"
uuid = "d96e819e-fc66-5662-9728-84c9c7592b0a"
version = "0.12.3"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "a6062fe4063cdafe78f4a0a81cfffb89721b30e7"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.2"

[[deps.PrettyTables]]
deps = ["Crayons", "Formatting", "Markdown", "Reexport", "Tables"]
git-tree-sha1 = "dfb54c4e414caa595a1f2ed759b160f5a3ddcba5"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "1.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Setfield]]
deps = ["ConstructionBase", "Future", "MacroTools", "Requires"]
git-tree-sha1 = "38d88503f695eb0301479bc9b0d4320b378bafe5"
uuid = "efcf1570-3423-57d1-acb7-fd33fddbac46"
version = "0.8.2"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.UnPack]]
git-tree-sha1 = "387c1f73762231e86e0c9c5443ce3b4a0a9a0c2b"
uuid = "3a884ed6-31ef-47d7-9d2a-63182c4928ed"
version = "1.0.2"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═396ec0e2-c5a8-11ec-2c94-b9b42bf7d13b
# ╠═c70c8b04-166a-480c-acb7-7a4efcb0930f
# ╠═6d41d5f0-2d61-4240-bc90-88f68c7bff56
# ╠═283bfa9e-833b-4754-b8b3-2229c8b2aafc
# ╠═1e527b64-9100-4a70-82f8-02c95835d059
# ╟─8f52bbd5-e4b1-4e24-a5ea-7bbebbb47f50
# ╠═15b0d459-6e9b-4493-834e-5a8dbe28a855
# ╠═eb4ea3c8-c5af-4731-b4cb-98a9835e3963
# ╠═0f611ea5-429c-4ced-84a2-476654ca3441
# ╠═f442268a-0420-4d10-91da-b6c3a0b601bf
# ╠═4ccdb7fe-0b24-4905-8939-8c580cbf3102
# ╠═504ff72a-07dd-49e8-ac4d-952f3e59e536
# ╠═d8fce66d-7e34-46dd-ba5d-efa5c814a271
# ╠═dbf62d40-918a-40ad-8f5e-df9f77770925
# ╠═b59ce9ca-719f-45b1-85bd-c0835dfdef0b
# ╠═16dde0f6-aee0-4255-b6e9-0491742fdd7b
# ╠═bdd0b535-e129-40d4-8468-6bbc802e8dc5
# ╠═ee527978-ac79-49a3-84ab-828788cb0964
# ╠═13b277c3-3227-46dc-a672-4d2542cecbe5
# ╠═ee826cb5-2d51-4518-90a8-c682db3d4c62
# ╠═ce318d63-5ad2-4741-bd1d-ca96d6e286d5
# ╠═fc7e7288-e49b-4e63-9247-e684c6bd5be9
# ╠═eb2a0e23-4c32-455d-a56e-e9cb6792c49a
# ╠═5343b156-b0b7-41c0-9809-4564c22be5d0
# ╠═4e5770e5-83e4-4f6a-b33c-cad4a5c87f3b
# ╠═2f97b7c2-d1cf-4f40-81b0-9b9ca32a2fca
# ╠═da393251-b601-4f5a-8d3e-82eba0655983
# ╠═9b4928b9-e4b9-41bd-a95b-d9877f47b90f
# ╠═32339ebd-eea9-431c-a990-9e7ada45a58a
# ╠═9e8d0d3e-40e8-40d3-bbc1-6f2574c76a0c
# ╠═130d554f-765c-4840-a254-ec7fae6a4dd3
# ╠═93f7beef-0a81-4cb5-939b-1a3707a573c1
# ╠═83ba4dc7-b735-4c7c-aecb-3cec6b27e669
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
